﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadsScript : StaticObjectScript
{

    public int Lenght = 40;
    public override void Start()
    {
        base.Start();
        LevelManagerScript.current.SetNewObstacles(transform);
    }
    public override void Action()
    {
        base.Action();
        LevelManagerScript.current.SetNewObstacles(transform);
    }
}
