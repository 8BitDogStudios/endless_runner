﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {



    //-----------------------------------------------------------------------------------------
    // Delegates:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Constants:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Inspector Variables:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Public Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Protected Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Public Variables:
    //-----------------------------------------------------------------------------------------
    

    //-----------------------------------------------------------------------------------------
    // Protected Variables:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Variables:
    //-----------------------------------------------------------------------------------------
    [SerializeField]
    [Range(1, 10)]
    private float Speed;
    private MovementScript Ms;
    private PlayerAnimScript Pas;
    private IEnumerator RunCoroutine;
    private PlayerAnimState currentState = PlayerAnimState.Idle;
    private InputState LastinputState = InputState.None;
    private PlayerState CurrentPlayerState = PlayerState.Run;
    [SerializeField]
    private List<ElementType> Inventory = new List<ElementType>();
    //-----------------------------------------------------------------------------------------
    // Unity Lifecycle:
    //-----------------------------------------------------------------------------------------

    private void Awake()
    {
        Ms = GetComponentInChildren<MovementScript>();
        Pas = GetComponentInChildren<PlayerAnimScript>();
    }
    // Use this for initialization
    void Start()
    {

        RunCoroutine = RunCo();
        currentState = PlayerAnimState.Run;
        Pas.SetAnimByState(currentState);
        StartCoroutine(RunCoroutine);
    }

    // Update is called once per frame
    void Update()
    {
    }

    //-----------------------------------------------------------------------------------------
    // Public Methods:
    //-----------------------------------------------------------------------------------------
    public void SetAnim(PlayerAnimState ps)
    {
        currentState = ps;
        Pas.SetAnimByState(currentState);
    }

    public InputState GetLastInputState()
    {
        return LastinputState;
    }

    public void SetLastInputStateWithoutAnim(InputState istate)
    {
         LastinputState = istate;
    }
    public void SetCurrentPlayerState(PlayerState playerState)
    {
        CurrentPlayerState = playerState;
    }

    public RowPosition GetCurrentRowPos()
    {
        return Ms.GetCurrentRowPos();
    }

    public void SetLastInputState(InputState inputState)
    {
        //Debug.Log("set last input" + inputState.ToString());
        LastinputState = inputState;
        switch (LastinputState)
        {
            case InputState.None:
                break;
            case InputState.Left:
                MoveLateral(LastinputState);
                break;
            case InputState.Right:
                MoveLateral(LastinputState);
                break;
            case InputState.Jump:
                Jump();
                break;
            case InputState.Crouch:
                Crouch();
                break;
            case InputState.Slide:
                Slide();
                break;
        }
    }

    public bool isElementInInventory(ElementType eleType)
    {
        bool isIn = Inventory.Contains(eleType) ? true : false;
        return isIn;
    }

    public void StopMoveCoroutine(float timeToStop)
    {
        StartCoroutine(StopCoroutineWithTime(timeToStop));
    }

    public void AddForceToObjects(Rigidbody[] rbs, float time)
    {
        StartCoroutine(AddForceToObjectsCo(rbs,time));
    }

    //-----------------------------------------------------------------------------------------
    // Protected Methods:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Methods:
    //-----------------------------------------------------------------------------------------

    private IEnumerator AddForceToObjectsCo(Rigidbody[] rbs, float time)
    {
        yield return new WaitForSeconds(time);
        foreach (Rigidbody rigidbody in rbs)
        {
            rigidbody.GetComponent<CogScript>().AfterExplosion();
            Vector3 explosionPos = new Vector3(transform.GetChild(0).position.x, 0, rigidbody.transform.position.z);
            rigidbody.AddExplosionForce(500, explosionPos, 50);
        }
    }

    private IEnumerator StopCoroutineWithTime(float timeToStop)
    {
        yield return new WaitForSeconds(timeToStop);
        StopCoroutine(RunCoroutine);
    }

    private void Jump()
    {
        if (CurrentPlayerState != PlayerState.CloseToObstacle)
        {
            SetAnim(PlayerAnimState.Jump);
        }
    }

    private void Crouch()
    {
        if (CurrentPlayerState != PlayerState.CloseToObstacle)
        {
            SetAnim(PlayerAnimState.Crouch);
        }
    }

    private void Slide()
    {
        if (CurrentPlayerState != PlayerState.CloseToObstacle)
        {
            SetAnim(PlayerAnimState.Slide);
        }
    }

    private void MoveLateral(InputState inputS)
    {
        if (inputS == InputState.Left)
        {
            Debug.Log("Left");
            Ms.Move(RowPosition.Left);
        }
        else if (inputS == InputState.Right)
        {
            Debug.Log("Right");
            Ms.Move(RowPosition.Right);
        }
    }

    private IEnumerator RunCo()
    {
        while (true)
        {
            transform.position += transform.forward * (Time.deltaTime * Speed);
            //Debug.Log(transform.position);
            yield return new WaitForEndOfFrame();
        }
    }
}


