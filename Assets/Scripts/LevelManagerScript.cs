﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManagerScript : MonoBehaviour {

    //-----------------------------------------------------------------------------------------
    // Delegates:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Constants:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Inspector Variables:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Public Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Protected Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Public Variables:
    //-----------------------------------------------------------------------------------------
    public static LevelManagerScript current;
    //-----------------------------------------------------------------------------------------
    // Protected Variables:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Variables:
    //-----------------------------------------------------------------------------------------


    [SerializeField]
    private int PlayerLevel = 0;


    #region Obstacles
    [System.Serializable]
    public class Obstacles
    {
        [Header("Obstacles Container")]
        [SerializeField]
        private Transform ObstaclesContainer;
        [Header("Low Obatsacles")]
        [SerializeField]
        private List<GameObject> LowObstacle = new List<GameObject>();
        [Header("Mid Obstacles")]
        [SerializeField]
        private List<GameObject> MidObstacle = new List<GameObject>();
        [Header("High Obstacles")]
        [SerializeField]
        private List<GameObject> HighObstacle = new List<GameObject>();
        [Header("Interactable Obstacles")]
        [SerializeField]
        private List<GameObject> InteractableObstacle = new List<GameObject>();
        private List<List<GameObject>> ObstaclesList = new List<List<GameObject>>();

        public Obstacles()
        {
            ObstaclesList.Add(LowObstacle);
            ObstaclesList.Add(MidObstacle);
            ObstaclesList.Add(HighObstacle);
        }
        public Transform GetContainer()
        {
            return ObstaclesContainer;
        }

        public void SetObjectsToContainer()
        {
            for (int i = 0; i < 3; i++)
            {
                foreach (List<GameObject> ListOfGO in ObstaclesList)
                {
                    foreach (GameObject obstacle in ListOfGO)
                    {
                        GameObject go = Instantiate(obstacle);
                        go.transform.parent = ObstaclesContainer;

                    }
                }
            }
        }
    }
    public Obstacles obstacles = new Obstacles();
    #endregion


    //-----------------------------------------------------------------------------------------
    // Unity Lifecycle:
    //-----------------------------------------------------------------------------------------

    private void Awake()
    {
        current = this;
        obstacles.SetObjectsToContainer();

    }
    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    //-----------------------------------------------------------------------------------------
    // Public Methods:
    //-----------------------------------------------------------------------------------------
    public void SetNewObstacles(Transform Road)
    {
        SetObstacleToObstacleContainer(Road);
        SetObjectsToRoad(Road);
    }
    //-----------------------------------------------------------------------------------------
    // Protected Methods:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Methods:
    //-----------------------------------------------------------------------------------------
    private void SetObstacleToObstacleContainer(Transform Road)
    {
        foreach (Transform child in Road)
        {
            child.parent = obstacles.GetContainer();
        }
    }

    private void SetObjectsToRoad(Transform Road)
    {
        int children = obstacles.GetContainer().childCount;
        if (children > 0)
        {
            for (int i = 0; i < PlayerLevel; i++)
            {
                Transform obstacle = obstacles.GetContainer().GetChild(Random.Range(0, children));
                obstacle.parent = Road;
                obstacle.localPosition = new Vector3(0, 0, 0);
            }
        }
    }
}
