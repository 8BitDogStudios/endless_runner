using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
	public enum AxisOption
	{
		// Options for which axes to use
		Both, // Use both
		OnlyHorizontal, // Only horizontal
		OnlyVertical // Only vertical
	}

	public int MovementRange = 100;
	public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
	public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
	public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input
    [SerializeField]
    [Range(50,500)]
    private float OffsetDrag = 50;
	Vector3 m_StartPos = Vector3.zero;
    Vector3 offset;
	bool m_UseX; // Toggle for using the x axis
	bool m_UseY; // Toggle for using the Y axis
	CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
	CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input

	void OnEnable()
	{
		CreateVirtualAxes();
	}

    void Start()
    {
    }
    private void Update()
    {
            if (Input.GetKeyDown(KeyCode.D))
            {
                //Debug.Log("Dx");
                InputHandlerScript.current.UserInput(UserInputType.Right);
            }
            else if (Input.GetKeyDown(KeyCode.A))
            {
                //Debug.Log("Sx");
                InputHandlerScript.current.UserInput(UserInputType.Left);
            }
        
            else if (Input.GetKeyDown(KeyCode.W))
            {
            
                //Debug.Log("Alto");
                InputHandlerScript.current.UserInput(UserInputType.Up);
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                //Debug.Log("Basso");
                InputHandlerScript.current.UserInput(UserInputType.Down);
            }

    }

    void UpdateVirtualAxes(Vector3 value)
	{
		var delta = m_StartPos - value;
		delta.y = -delta.y;
		delta /= MovementRange;
		if (m_UseX)
		{
			m_HorizontalVirtualAxis.Update(-delta.x);
		}

		if (m_UseY)
		{
			m_VerticalVirtualAxis.Update(delta.y);
		}
	}

	void CreateVirtualAxes()
	{
		// set axes to use
		m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
		m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

		// create new axes based on axes to use
		if (m_UseX)
		{
			m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
			CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
		}
		if (m_UseY)
		{
			m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
			CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
		}
	}


	public void OnDrag(PointerEventData data)
	{
        Vector3 newPos = Vector3.zero;
        
        if (m_UseX)
        {
            int delta = (int)(data.position.x - m_StartPos.x);
            newPos.x = delta;
        }

        if (m_UseY)
        {
            int delta = (int)(data.position.y - m_StartPos.y);
            newPos.y = delta;
        }

        UpdateVirtualAxes(Vector3.zero);
	}


	public void OnPointerUp(PointerEventData data)
	{

        float xDir = Math.Abs(data.position.x - offset.x);
        float yDir = Math.Abs(data.position.y - offset.y);

        if (xDir > yDir)
        {
            if (data.position.x - offset.x > OffsetDrag)
            {
                //Debug.Log("Dx");
                InputHandlerScript.current.UserInput(UserInputType.Right);
            }
            else if (offset.x - data.position.x > OffsetDrag)
            {
                //Debug.Log("Sx");
                InputHandlerScript.current.UserInput(UserInputType.Left);
            }
        }
        else
        {
            if (data.position.y - offset.y > OffsetDrag)
            {
                //Debug.Log("Alto");
                InputHandlerScript.current.UserInput(UserInputType.Up);
            }
            else if (offset.y - data.position.y > OffsetDrag)
            {
                //Debug.Log("Basso");
                InputHandlerScript.current.UserInput(UserInputType.Down);
            }

        }
        
        
        UpdateVirtualAxes(m_StartPos);
    }


	public void OnPointerDown(PointerEventData data)
    {
        offset = data.position;
    }

	void OnDisable()
	{
		// remove the joysticks from the cross platform input
		if (m_UseX)
		{
			m_HorizontalVirtualAxis.Remove();
		}
		if (m_UseY)
		{
			m_VerticalVirtualAxis.Remove();
		}
	}
}