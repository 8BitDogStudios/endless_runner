﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterctableObjectBase : MonoBehaviour {
    [SerializeField]
    private RowPosition pos;

    public virtual void Action()
    {

    }

    public RowPosition GetPos()
    {
        return pos;
    }
}
