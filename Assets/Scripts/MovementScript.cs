﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour {


    //-----------------------------------------------------------------------------------------
    // Delegates:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Constants:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Inspector Variables:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Public Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Protected Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Public Variables:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Protected Variables:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Variables:
    //-----------------------------------------------------------------------------------------
    private IEnumerator MoveCoroutine;
    [SerializeField]
    [Range(1, 10)]
    private float Speed;
    private RowPosition currentRow = RowPosition.Central;
    //-----------------------------------------------------------------------------------------
    // Unity Lifecycle:
    //-----------------------------------------------------------------------------------------

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    //-----------------------------------------------------------------------------------------
    // Public Methods:
    //-----------------------------------------------------------------------------------------

    public void Move(RowPosition pos)
    {
        switch (pos)
        {
            case RowPosition.Left:
                if (currentRow != RowPosition.Left)
                {
                    currentRow -=1;
                    StartCoroutine(MoveCo(-transform.right));
                }
                break;
            case RowPosition.Right:
                if (currentRow != RowPosition.Right)
                {
                    currentRow += 1;
                    StartCoroutine(MoveCo(transform.right));
                }
                break;
        }
    }


    public RowPosition GetCurrentRowPos()
    {
        return currentRow;
    }
    
    //-----------------------------------------------------------------------------------------
    // Protected Methods:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Methods:
    //-----------------------------------------------------------------------------------------
    private IEnumerator MoveCo(Vector3 dir)
    {
        int i = 0;
        bool isRightPos = false;
        int xOffset = (int)transform.localPosition.x;
        while (!isRightPos)
        {
            transform.localPosition += (dir * (Time.deltaTime * Speed));
            if (dir.x > 0 && transform.localPosition.x > (xOffset + dir.x))
            {
                transform.localPosition = new Vector3(xOffset + dir.x, 0, 0);
                isRightPos = true;
            }
            else if (dir.x < 0 && transform.localPosition.x < (xOffset + dir.x))
            {
                transform.localPosition = new Vector3(xOffset + dir.x, 0, 0);
                isRightPos = true;

            }
            //Debug.Log(transform.localPosition);
            yield return new WaitForEndOfFrame();
            i++;
        }
    }
}
