﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandlerScript : MonoBehaviour {


    //-----------------------------------------------------------------------------------------
    // Delegates:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Constants:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Inspector Variables:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Public Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Protected Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Properties:
    //-----------------------------------------------------------------------------------------
    private float ImputTimeOffset;
    private PlayerScript playerscript;
    private UserInputType _LastInput;
    private UserInputType LastInput
    {
        get
        {
            //Debug.Log(Time.time - ImputTimeOffset + "Sottrazione");
            //Debug.Log(ImputTimeOffset);
            if (Time.time - ImputTimeOffset < 1.5f)
            {
                return _LastInput;
            }
            else
            {
                return UserInputType.None;

            }
        }
        set
        {
            ImputTimeOffset = Time.time;
            _LastInput = value;
        }
    }
    //-----------------------------------------------------------------------------------------
    // Public Variables:
    //-----------------------------------------------------------------------------------------
    public static InputHandlerScript current;
    //-----------------------------------------------------------------------------------------
    // Protected Variables:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Variables:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Unity Lifecycle:
    //-----------------------------------------------------------------------------------------
    // Use this for initialization
    void Start()
    {
        current = this;
        playerscript = GetComponent<PlayerScript>();
        ImputTimeOffset = Time.time;
    }

    // Update is called once per frame
    void Update()
    {

    }
    //-----------------------------------------------------------------------------------------
    // Public Methods:
    //-----------------------------------------------------------------------------------------
    public void UserInput(UserInputType userInputType)
    {
        switch (userInputType)
        {
            case UserInputType.None:
                break;
            case UserInputType.Up:
                playerscript.SetLastInputState(InputState.Jump);
                break;
            case UserInputType.Left:
                playerscript.SetLastInputState(InputState.Left);
                break;
            case UserInputType.Right:
                playerscript.SetLastInputState(InputState.Right);
                break;
            case UserInputType.Down:
                //Debug.Log(LastInput.ToString());
                if (LastInput == UserInputType.Down)
                {
                    playerscript.SetLastInputState(InputState.Slide);
                }
                else
                {
                    playerscript.SetLastInputState(InputState.Crouch);
                }
                break;
        }
        LastInput = userInputType;
    }
    //-----------------------------------------------------------------------------------------
    // Protected Methods:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Methods:
    //-----------------------------------------------------------------------------------------
}
