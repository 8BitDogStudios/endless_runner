﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(BoxCollider))]
public class ObstacleBase : MonoBehaviour
{


    

    //-----------------------------------------------------------------------------------------
    // Delegates:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Constants:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Inspector Variables:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Public Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Protected Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Public Variables:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Protected Variables:
    //-----------------------------------------------------------------------------------------
    protected PlayerScript playerScript;
    //-----------------------------------------------------------------------------------------
    // Private Variables:
    //-----------------------------------------------------------------------------------------
    private bool isAnimationDone = false;
    [SerializeField]
    private OverTakeType overTakeT;
    [SerializeField]
    private DeathType DeathT;
    [SerializeField]
    private InputState[] ObjOverTakeInput;
    [SerializeField]
    [Range(0, 2)]
    private float TimeToStop = 0.5f;
    [SerializeField]
    private float explosionTime;
    [SerializeField]
    private ElementType eleType;
    //-----------------------------------------------------------------------------------------
    // Unity Lifecycle:
    //-----------------------------------------------------------------------------------------
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player" && !isAnimationDone)
        {
            Debug.Log("entrato");
            if (playerScript == null)
            {
                playerScript = col.GetComponentInParent<PlayerScript>();
            }
            if (ObjOverTakeInput.Contains(InputState.None))
            {
                CheckForElement();

            }
            playerScript.SetLastInputState(InputState.None);
            playerScript.SetCurrentPlayerState(PlayerState.CloseToObstacle);
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player" && !isAnimationDone)
        {
            //Debug.Log("uscito");
            isAnimationDone = true;
            if (ObjOverTakeInput.Contains(playerScript.GetLastInputState()))
            {

                switch (overTakeT)
                {
                    case OverTakeType.Jump:
                        playerScript.SetAnim(PlayerAnimState.Jump);
                        break;
                    case OverTakeType.JumpLow:
                        playerScript.SetAnim(PlayerAnimState.JumpingLow);
                        break;
                    case OverTakeType.JumpHigh:
                        playerScript.SetAnim(PlayerAnimState.JumpingHigh);
                        break;
                    case OverTakeType.Crouch:
                        playerScript.SetAnim(PlayerAnimState.Crouch);
                        break;
                    case OverTakeType.Slide:
                        playerScript.SetAnim(PlayerAnimState.Slide);
                        break;
                    case OverTakeType.ThrowObject:
                        playerScript.SetAnim(PlayerAnimState.ThrowObject);
                        playerScript.AddForceToObjects(GetComponentsInChildren<Rigidbody>().Where(r=> r.GetComponent<CogScript>() != null).ToArray(), explosionTime);
                        break;
                    case OverTakeType.None:
                        CheckForIntercatablePosition();
                        break;
                }
                playerScript.SetCurrentPlayerState(PlayerState.Run);
            }
            else
            {
                SetDeathAnim();
            }
        }
    }


    //-----------------------------------------------------------------------------------------
    // Public Methods:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Protected Methods:
    //-----------------------------------------------------------------------------------------
    
    //-----------------------------------------------------------------------------------------
    // Private Methods:
    //-----------------------------------------------------------------------------------------
    private void CheckForElement()
    {
        if (playerScript.isElementInInventory(eleType))
        {
            GetComponentInChildren<InterctableObjectBase>().Action();
            playerScript.SetLastInputStateWithoutAnim(InputState.None);

        }
        else
        {
            StartCoroutine(WaitToSetDeath());

        }
    }

    private IEnumerator WaitToSetDeath()
    {

        yield return new WaitForSeconds(0.3f);
        playerScript.SetLastInputStateWithoutAnim(InputState.Jump);
    }

    private void CheckForIntercatablePosition()
    {
        if(GetComponentInChildren<InterctableObjectBase>().GetPos() != playerScript.GetCurrentRowPos())
        {
            Debug.Log("none");
            SetDeathAnim();
        }
    }

    private void SetDeathAnim()
    {
        playerScript.StopMoveCoroutine(TimeToStop);
        switch (DeathT)
        {
            case DeathType.Low:
                playerScript.SetAnim(PlayerAnimState.DieLow);
                break;
            case DeathType.Mid:
                playerScript.SetAnim(PlayerAnimState.DieMid);
                break;
            case DeathType.High:
                playerScript.SetAnim(PlayerAnimState.DieHigh);
                break;
        }
    }
}


