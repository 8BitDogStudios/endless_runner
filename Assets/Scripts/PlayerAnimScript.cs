﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimScript : MonoBehaviour {

    //-----------------------------------------------------------------------------------------
    // Delegates:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Constants:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Inspector Variables:
    //-----------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------
    // Public Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Protected Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Properties:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Public Variables:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Protected Variables:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Variables:
    //-----------------------------------------------------------------------------------------
    private Animator Anim;

    public Transform himp;
    //-----------------------------------------------------------------------------------------
    // Unity Lifecycle:
    //-----------------------------------------------------------------------------------------
    private void Awake()
    {
        Anim = GetComponent<Animator>();
    }
    // Use this for initialization
    void Start()
    {
        //StartCoroutine(t());
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, 0, -himp.localPosition.z);
    }

    //-----------------------------------------------------------------------------------------
    // Public Methods:
    //-----------------------------------------------------------------------------------------
    public void SetAnimByState(PlayerAnimState playerstate)
    {
        switch (playerstate)
        {
            case PlayerAnimState.Idle:
                Anim.SetInteger("State", 0);
                break;
            case PlayerAnimState.Dance:
                Anim.SetInteger("State", 1);
                break;
            case PlayerAnimState.Run:
                Anim.SetInteger("State", 2);
                break;
            case PlayerAnimState.Crouch:
                Anim.SetInteger("State", 3);
                StartCoroutine(SetToRunState());
                break;
            case PlayerAnimState.Slide:
                Anim.SetInteger("State", 4);
                StartCoroutine(SetToRunState());
                break;
            case PlayerAnimState.Jump:
                Anim.SetInteger("State", 5);
                StartCoroutine(SetToRunState());
                break;
            case PlayerAnimState.JumpingLow:
                Anim.SetInteger("State", 6);
                StartCoroutine(SetToRunState());
                break;
            case PlayerAnimState.JumpingHigh:
                Anim.SetInteger("State", 7);
                StartCoroutine(SetToRunState());
                break;
            case PlayerAnimState.DieLow:
                Debug.Log("Low");
                Anim.SetInteger("State", 8);
                StartCoroutine(SetToDeathState());
                break;
            case PlayerAnimState.DieMid:
                Anim.SetInteger("State", 9);
                StartCoroutine(SetToDeathState());
                break;
            case PlayerAnimState.DieHigh:
                Anim.SetInteger("State", 10);
                StartCoroutine(SetToDeathState());
                break;
            case PlayerAnimState.ThrowObject:
                Anim.SetInteger("State", 11);
                StartCoroutine(SetToRunState());
                break;
        }
    }
    //-----------------------------------------------------------------------------------------
    // Protected Methods:
    //-----------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------------
    // Private Methods:
    //-----------------------------------------------------------------------------------------

    private IEnumerator SetToRunState()
    {
        yield return new WaitForSeconds(0.1f);
        Anim.SetInteger("State", 2);
    }

    private IEnumerator SetToDeathState()
    {
        Debug.Log("isdeath");
        yield return new WaitForEndOfFrame();
        Anim.SetBool("IsDeath", true);
    }
}
