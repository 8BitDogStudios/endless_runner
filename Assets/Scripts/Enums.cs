﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enums : MonoBehaviour {

    
}


public enum RowPosition
{
    Left = 0,
    Central,
    Right = 2
}

public enum ElementType
{
    Hammer = 0,
    Key = 1
}

public enum InputState
{
    None = 0,
    Left,
    Right,
    Jump,
    Crouch,
    Slide = 5
}

public enum PlayerAnimState
{
    Idle = 0,
    Dance,
    Run,
    Crouch,
    Slide,
    Jump,
    JumpingLow,
    JumpingHigh,
    DieLow,
    DieMid,
    DieHigh,
    ThrowObject = 11
}

public enum OverTakeType
{
    Jump = 0,
    JumpLow,
    JumpHigh,
    Crouch,
    Slide,
    ThrowObject,
    None = 6
}

public enum DeathType
{
    Low = 0,
    Mid,
    High = 2
}

public enum UserInputType
{
    None = 0,
    Up,
    Left,
    Right,
    Down = 4
}

public enum PlayerState
{
    Run = 0,
    CloseToObstacle,
    Animation = 2
}